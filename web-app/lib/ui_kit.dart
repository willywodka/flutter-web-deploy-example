import 'package:flutter/material.dart';

class UiKit {
  static const TextStyle headerText = TextStyle(fontSize: 18,color: Colors.white);
  static const TextStyle centerBodyText = TextStyle(fontSize: 24);
}