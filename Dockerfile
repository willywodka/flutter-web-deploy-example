# Выбираем обрвз debian для сборки
FROM ubuntu:latest AS build-env


# Устанавливаем brew для загрузки fvm
RUN apt-get update && apt-get install -y git curl binutils clang make
RUN git clone https://github.com/Homebrew/brew.git /home/linuxbrew/.linuxbrew/Homebrew \
&& mkdir /home/linuxbrew/.linuxbrew/bin \
&& ln -s ../Homebrew/bin/brew /home/linuxbrew/.linuxbrew/bin \
&& eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv) \
&& brew --version
ENV PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:${PATH}"


RUN brew update
RUN brew doctor

# #Загружаем необходимые зависимости для компилятора
RUN apt-get update
RUN apt-get install -y fonts-droid-fallback
RUN apt-get install -y curl git wget unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa lib32stdc++6 python3 sed
RUN apt-get clean

#Устанавливаем fvm
RUN brew tap leoafarias/fvm
RUN brew install fvm

ENV PATH="${PATH}:/fvm"

#RUN git clone https://github.com/flutter/flutter.git /usr/local/flutter



#Запускаем и обновляем flutter
#RUN fvm flutter doctor -v
#RUN flutter channel master
#RUN flutter upgrade

#RUN fvm

#Создаем папки для дальнейшей работы
RUN mkdir /app/
COPY ./web-app/. /app/
WORKDIR /app/

RUN fvm install

# RUN fvm install 3.10.2
RUN fvm flutter doctor -v

#Запускаем компиляцию
RUN fvm flutter build web

#Берем образ с nginx
FROM nginx:alpine
#Копируем скомпилированное flutter web приложение в контейнер с nginx
COPY --from=build-env /app/build/web /usr/share/nginx/html
#ADD web-page /usr/share/nginx/html/